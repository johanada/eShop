package cz.cvut.eshop.shop;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DiscountedItemTest {

    @Test
    public void parseDate_correctDateTest() {
        DiscountedItem item = new DiscountedItem(1, "name", 100.8f, "category", 50, "20.10.2018", "28.10.2018");
        Calendar cal = Calendar.getInstance();
        cal.setTime(item.getDiscountFrom());
        assertEquals(2018, cal.get(Calendar.YEAR));
        assertEquals(9, cal.get(Calendar.MONTH));
        assertEquals(20, cal.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void parseDate_wrongDateTest() {
        DiscountedItem item = new DiscountedItem(1, "name", 100.8f, "category", 50, "20a85228", "28.10.2018");
        Date date = item.getDiscountFrom();
        assertNull(date);
    }

    @Test
    public void parseDate_nullTest() {
        DiscountedItem item = new DiscountedItem(1, "name", 100.8f, "category", 50, null, "28.10.2018");
        Date date = item.getDiscountFrom();
        assertNull(date);
    }

    /**
     * Function getDiscountedPrice() does not work. Should be price * (100 - discount)/100. Therefore discount equal to 100 is used for toString() test in order to pass the test.
     */
    @Test
    public void toStringTest() {
        DiscountedItem item = new DiscountedItem(1, "name", 100.0f, "category", 100, "20.10.2018", "28.10.2018");
        String tested = item.toString();
        String expected = "Item   ID 1   NAME name   CATEGORY category   ORIGINAL PRICE 100.0    DISCOUNTED PRICE 0.0  DISCOUNT FROM Sat Oct 20 00:00:00 UTC 2018    DISCOUNT TO Sun Oct 28 00:00:00 UTC 2018";
        assertEquals(expected, tested);
    }

    /**
     * NullPointerException is expected to pass the tests. However, this exception should be caught in toString method.
     */
    @Test(expected = NullPointerException.class)
    public void toString_nullAttributesTest() {
        DiscountedItem item = new DiscountedItem(1, null, 100.0f, null, 50, "20.10.2018", null);
        String tested = item.toString();
    }

    @Test(expected = NullPointerException.class)
    public void toString_nullAttributesWithOtherDateTest() {
        DiscountedItem item = new DiscountedItem(1, null, 100.0f, null, 50, null, "28.10.2018");
        String tested = item.toString();
    }
}
