package cz.cvut.eshop.shop;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;

import static org.junit.Assert.*;

public class ShoppingCartTest {

    @Test
    public void getCartItems_insertNewAndGet_shouldReturnInsertedOne() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);

        assertEquals(1, shCart.getCartItems().size());

        Iterator<Item> it = shCart.getCartItems().iterator();
        assertEquals(item.getID(), it.next().getID());
        assertFalse(it.hasNext());

    }

    @Test
    public void removeItem_insertAndRemove_itemsSizeShouldBeZero() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);
        assertEquals(1, shCart.getCartItems().size());

        shCart.removeItem(item.getID());
        assertEquals(0, shCart.getCartItems().size());

    }

    @Test
    public void getItemsCount_insertTwoItems_shouldBeTwo() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);
        shCart.addItem(item);

        assertEquals(2, shCart.getItemsCount());
    }

    @Test
    public void getTotalPrice_insertOneItem_shouldBeSameAsItemPrice() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);

        assertEquals(item.getPrice(), shCart.getTotalPrice(),0.05f);
    }

    @Test
    public void getTotalPrice_insertNone_shouldBeZero() {
        ShoppingCart shCart = new ShoppingCart();

        assertEquals(0f, shCart.getTotalPrice(),0.05f);
    }

    @Test
    public void constructorTest(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        //assert
        assertNotNull(shopCart);
    }

    @Test
    public void howManyItemsAreInTheCart_noItemSupposedToBe(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        ArrayList<Item> actual = shopCart.getCartItems();
        assertEquals(0, actual.size());
        //act
        shopCart.getCartItems();
    }

    @Test
    public void howManyItemsInTheCart_oneItemIsSupposedToBe(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item = new StandardItem(1, "name", 3.14f,"category",10);
        //act
        shopCart.addItem(item);
        int actual = shopCart.getItemsCount();
        //assert
        assertEquals(1, actual);
    }

    @Test
    public void whatIsThePriceOfCart_20(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item1 = new StandardItem(1, "name1", 1,"category",10);
        Item item2 = new StandardItem(2, "name2", 2,"category",20);
        Item item3 = new StandardItem(3, "name3", 17,"category",30);
        //act
        shopCart.addItem(item1);
        shopCart.addItem(item2);
        shopCart.addItem(item3);
        float price = shopCart.getTotalPrice();
        //assert
        assertEquals(20,price, 0);
    }

    @Test
    public void isItemRemoved_2(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item1 = new StandardItem(1, "name1", 1,"category",10);
        Item item2 = new StandardItem(2, "name2", 2,"category",20);
        Item item3 = new StandardItem(3, "name3", 17,"category",30);
        //act
        shopCart.addItem(item1);
        shopCart.addItem(item2);
        shopCart.addItem(item3);
        int numberOfItems1 = shopCart.getItemsCount();
        //assert
        assertEquals(3,numberOfItems1);
        //act
        //BYLO POTREBA UPRAVIT PUVODNI FUNKCI removeItem(), JINAK NEFUNGOVALA A HAZELA CHYBY
        shopCart.removeItem(2);
        //assert
        int numberOfItems2 = shopCart.getItemsCount();
        assertEquals(2,numberOfItems2);
    }

}